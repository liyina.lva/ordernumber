# -*- coding: utf-8 -*-
import types


class StrategyOrder:
    def __init__(self, func=None):
        self.name = 'Strategy order number'
        self.list_number = []
        if func is not None:
            self.execute = types.MethodType(func, self)

    def execute(self):
        print(self.name)


def execute_order_asc(self):
    print(self.name + 'asc')
    self.list_number.sort()


def execute_order_desc(self):
    print(self.name + 'desc')
    self.list_number.sort(reverse=True)


def execute_oder_asc_manual(self):
    print(self.name + 'asc manual')
    for route in range(1, len(self.list_number)):
        for position in range(len(self.list_number) - route):
            if self.list_number[position] > self.list_number[position + 1]:
                temp = self.list_number[position]
                self.list_number[position] = self.list_number[position + 1]
                self.list_number[position + 1] = temp
