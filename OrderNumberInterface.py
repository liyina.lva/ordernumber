# -*- coding: utf-8 -*-
import types
import StrategyOrder
from HelperValidatorNumber import HelperValidatorNumber

STRATEGY_ORDER_TEMPLATES = {
    '1': StrategyOrder.execute_order_asc,
    '2': StrategyOrder.execute_order_desc,
    '3': StrategyOrder.execute_oder_asc_manual
}

class OrderNumberInterface:

    def __init__(self, func=None):
        print 'Ordenador de números \n   '
        if func is not None:
            self.execute = types.MethodType(func, self)

    def execute(self):
        p = 1
        list_number = []
        vlr_tmp = 0
        temp = 0

        while p > 0:
            norg = self.number_to_order()
            self.to_enter_number(norg, list_number)
            rep = self.choose_ordering_method()
            rep = self.validate_choose_ordering_method(rep)
            self.order_number_strategy_template(list_number, rep)
            self.message_number_list(list_number)
            rep = raw_input('Desea repetir el programa?(si o no) ')
            rep = self.validate_if_exit(rep)
            list_number, vlr_tmp = self.restart(list_number, rep, vlr_tmp)
            if rep == 'no':
                print '\nGracias por usar nuestro programa. \n \nDesarrollado por: \n Liyina Veizaga'
                break

    def order_number_strategy_template(self, list_number, rep):
        strat = StrategyOrder.StrategyOrder(STRATEGY_ORDER_TEMPLATES[str(rep)])
        strat.list_number = list_number
        strat.execute()

    @staticmethod
    def number_to_order():
        while True:
            try:
                norg = raw_input('Cuantos números desea ordenar? \n->')
                norg = int(norg)
                break
            except:
                print 'Valor incorrecto. Vuelva a intentarlo.'
        return norg

    @staticmethod
    def to_enter_number(norg, list):
        global vlr_tmp
        for i in range(1, norg + 1):
            vlr_tmp = raw_input('Digite el campo número ' + str(i) + ': ')
            while HelperValidatorNumber.validate_number(norg, 'float') == 0:
                vlr_tmp = raw_input('ERROR, campo no numérico.\n Digite el campo número ' + str(i) + ': ')
            list.append(float(vlr_tmp))

    @staticmethod
    def choose_ordering_method():
        print 'Escoja un método de ordenamiento'
        print 'Ordenar ascendente (1)'
        print 'Ordenar descendente (2)'
        while True:
            try:
                rep = raw_input('Ordenar ascendente manual (3) \n->')
                rep = int(rep)
                break
            except:
                print 'Valor incorrecto. Vuelva a intentarlo.'
        return rep

    @staticmethod
    def validate_choose_ordering_method(rep):
        if rep not in [1, 2, 3]:
            rep = raw_input('Valor incorrecto. Vuelva a intentarlo. Tiene que escoger del 1 al 3')
        return rep

    @staticmethod
    def message_number_list(lista):
        print '   \n    '
        print 'Ordenados: ', lista
        print '    '

    @staticmethod
    def validate_if_exit(rep):
        while rep != 'si' and rep != 'no':
            rep = raw_input('Valor incorrecto. Vuelva a intentarlo. ')
        return rep

    @staticmethod
    def restart(lista, rep, vlr_tmp):
        if rep == 'si':
            print '\nReiniciando... \n     \n'
            lista = []
            vlr_tmp = 0
        return lista, vlr_tmp


if __name__ == '__main__':
    OrderNumberInterface = OrderNumberInterface()
    OrderNumberInterface.execute()

