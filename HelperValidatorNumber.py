# -*- coding: utf-8 -*-


class HelperValidatorNumber:

    def __init__(self):
        pass

    @staticmethod
    def validate_number(number, type_number):
        if type_number != 'int' and type_number != 'float':
            return 0  # error
        else:
            try:
                if type_number == 'int':
                    dato = int(number)
                else:
                    dato = float(number)
                return 1  # ok
            except:
                return 0  # error